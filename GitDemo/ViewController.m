//
//  ViewController.m
//  GitDemo
//
//  Created by RailsBox on 17/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    
    int i=1+1;
    NSLog(@"sum is i=%d",i);
    NSLog(@"sum is i=%d",i+5);
    
    [self sayhello];
    [self saybye];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)saybye
{
    NSLog(@"Bye Bye");
}

-(void)sayhello
{
    NSLog(@"Hello All");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
